import React from 'react'
import { View, Text } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'

import Splash from '../Pages/Splash'
import Ig from '../Pages/Ig'

const Stack = createStackNavigator();

const index = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}/>
            <Stack.Screen
            name="Ig"
            component={Ig}
            options={{headerShown: false}}/>

        </Stack.Navigator>
      
    )
}

export default index
