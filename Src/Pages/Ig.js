import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import Header from '../components/Header';
import Story from '../components/Story';
import Conten from '../components/Conten';
import Conten22 from '../components/Conten22';
import Conten33 from '../components/Conten33';
import Conten44 from '../components/Conten44';
import Conten55 from '../components/Conten55';

import Footer from '../components/Footer'; 

const App = () => {
    return (
        <View>
            <Header />
            <ScrollView>
                <Story />
                <Conten />
                <Conten22 />
                <Conten33 />
                <Conten44 />
                <Conten55 />

            </ScrollView>
            <Footer />
        </View>

        
    )
}

export default App
