import React,{useEffect} from 'react'
import { View, Text, StyleSheet,Image } from 'react-native'

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Ig');
        }, 3000);
    });
    return (
        <View style={Styles.Atas}>
        <View style={Styles.wrapper}>
            <Image source={require('../assets/lg.jpg')} style={{height:100,width:100}} />
            </View>
            <View style={Styles.coba}>
            <Text style={{fontSize:18,color:'gray'}}>form</Text>
            </View>
            <View style={Styles.logo}>
            <Image source={require('../assets/fb.png')}style={{height:120,width:120}}/>
            </View>
        </View>
    )
}

export default Splash

const Styles = StyleSheet.create({
    Atas: {
        backgroundColor:'white'
    },
        wrapper: {
            marginVertical:250,
            alignItems: 'center',
            justifyContent: 'center',
        },
        logo: {
            marginLeft:120
        },
        WelcomeText: {
            fontSize: 24,
            fontWeight: 'bold',
            color: 'green',
            paddingBottom: 20,
        },
        coba: {
            margin:0,
            padding:0,
            marginLeft:160

        }
        
});