import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'

import Route from './Route/index'
export default function App() {
    return (
        <NavigationContainer>
            <Route/>
        </NavigationContainer>
    )
}

const styles = StyleSheet.create({})
